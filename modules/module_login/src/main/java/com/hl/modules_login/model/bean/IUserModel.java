package com.hl.modules_login.model.bean;

public interface IUserModel{
    void getUser(String phone, String pass);
    void registerUser(String phone, String pass, String repass);
}
