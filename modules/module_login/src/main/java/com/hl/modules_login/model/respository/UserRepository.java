package com.hl.modules_login.model.respository;

import com.hl.lib_network.controller.presenter.BaseControlPresenter;
import com.hl.lib_network.net.response.TypeCallBack;
import com.hl.modules_login.model.bean.IUserModel;
import com.hl.modules_login.model.bean.UserBean;

public class UserRepository implements IUserModel {
    private BaseControlPresenter baseControlPresenter;

    public UserRepository(BaseControlPresenter baseControlPresenter){
        this.baseControlPresenter = baseControlPresenter;
    }
    @Override
    public void getUser(String phone, String pass) {
        // 请求网络获取User，这里可以用任何网络请求框架来操作；
        // 只要回调数据给 BaseControlContract.View 即可; 界面会继承BaseControlContract.View
        baseControlPresenter
                .addParam("username", phone)
                .addParam("password", pass)
                .postData(
                "/user/login",
                new TypeCallBack<UserBean>() {
                },
                null, true);
    }

    @Override
    public void registerUser(String phone, String pass, String repass) {
        // 请求网络获取User，这里可以用任何网络请求框架来操作；
        // 只要回调数据给 BaseControlContract.View 即可; 界面会继承BaseControlContract.View
        baseControlPresenter
                .addParam("username", phone)
                .addParam("password", pass)
                .addParam("repassword", repass)
                .postData(
                        "/user/register",
                        new TypeCallBack<UserBean>() {
                        },
                        null, true);
    }
}
