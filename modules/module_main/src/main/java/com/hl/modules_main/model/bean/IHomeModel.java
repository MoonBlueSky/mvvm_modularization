package com.hl.modules_main.model.bean;

public interface IHomeModel {
    void getHomeList(int page);
}
